# Importation
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect

from django.contrib import messages

from .forms import ImageForm
from .image_filter import *

from .models import *


# Pour récupérer l'objet UserData/ le créer s'il n'existe pas
def getUserData(request):
    u = UserData.objects.filter(user=request.user).count()
    if u == 0:
        UserData(user=request.user).save()
    return UserData.objects.get(user=request.user)


# Donne un contexte de base
def default_context(request):
    context = {}
    if request.user.is_authenticated:
        userdata = getUserData(request)
        if userdata.pdp: #photo de profile
            photo_url = userdata.pdp.url
            context['photo_index'] = photo_url
        if UserScore.objects.filter(user=request.user): #score perso
            scores = sorted(UserScore.objects.filter(user=request.user), key=get_my_key, reverse=True)
            context["scores"] = scores
    return context


# Fonction de tri
def get_my_key(obj):
    return obj.score


# Accueil (bojeu.fr)
def index(request):
    context = {}
    if request.user.is_authenticated:
        context = default_context(request)

    globalScoreWithPP = []
    for score in sorted(UserScore.objects.filter(), key=get_my_key, reverse=True):
        userdata = getUserData(score)
        photo_url = ""
        if userdata.pdp:
            photo_url = userdata.pdp.url
        globalScoreWithPP.append([score, photo_url])
    #Je fais une liste de user et de photo de profile pour plus de simplicité

    context["globalScores"] = globalScoreWithPP

    return render(request, 'index.html', context)


# À propos du site (/about)
def about(request):
    context = default_context(request)  # Récupération du contexte de base
    return render(request, 'about.html', context)  # Rendu basique


# Choix des Jeux (/games)
def games(request):
    context = default_context(request)  # Récupération du contexte de base
    return render(request, 'games.html', context)  # Rendu basique


# Page d’Inscription (/register)
def register(request):
    if request.user.is_authenticated:  # Vérification si le joueur est connecté
        return redirect("/account")  # Redirection vers la page de compte
    else:
        form = UserCreationForm()  # Création du Formulaire pour la création de compte

        if request.method == 'POST':
            form = UserCreationForm(request.POST)
            if form.is_valid():
                form.save()
                return redirect('/login')  # Redirection vers la page de connexion

        context = {'form': form}
        return render(request, 'account/register.html', context)  # Rendu basique


# Vérification de couleur
def checkColor(post, path):
    if 'black_and_white_filter' in post:
        BlackAndWhiteFilter(path)
    elif 'red_filter' in post:
        Rouge(path)
    elif 'green_filter' in post:
        Vert(path)
    elif 'blue_filter' in post:
        Bleu(path)
    elif 'yellow_filter' in post:
        Jaune(path)
    elif 'purple_filter' in post:
        Violet(path)
    elif 'turquoise_filter' in post:
        Turquoise(path)
    elif 'pink_filter' in post:
        Rose(path)
    elif 'orange_filter' in post:
        Orange(path)


# Page de Connexion (/login)
def loginPage(request):
    if request.user.is_authenticated:
        return redirect("/account")
    elif request.method == 'POST':
        #Récupération du nom et de mot de passe de l'utilisateur en requête POST
        username = request.POST.get("username")
        password = request.POST.get("password")

        user = authenticate(request, username=username, password=password)
        #Essaye d'authentifier

        if user is not None:
            #Si l'utilisateur existe alors il est connecté et redirigé vers la page de compte
            login(request, user)
            return redirect("/account")
        else:
            #Sinon il a un message d'erreur
            messages.info(request, 'Le nom ou le mot de passe est incorrecte')

    return render(request, 'account/login.html')


# Page de Déconnexion (/logout)
def logoutPage(request):
    logout(request)  # déconnexion
    return redirect("/login")  # redirection vers la page de connexion


# Page de Compte (/account)
def account(request):
    context = default_context(request)  # Récupération du contexte de base
    if request.user.is_authenticated:  # Vérification si le joueur est connecté
        userdata = getUserData(
            request)  # Permet d'avoir accès à l'utilisateur que j'ai custom qui contient donc la photo de profil
        form = ImageForm()  # Création du Formulaire pour l'importation d'image
        if 'pdp' in request.POST:  # Si la photo de profil est rempli
            form = ImageForm(request.POST, request.FILES)  # La récupérer
            if form.is_valid():
                img = form.cleaned_data.get('pdp')
                userdata.pdp = img
                userdata.save()  # Mettre à jour dans le modèle
                context['photo_index'] = userdata.pdp.url  # Update du context
        if userdata.pdp:
            checkColor(request.POST, userdata.pdp.path)
        context['form'] = form

        return render(request, 'account/account.html', context)  # Rendu basique
    else:
        return render(request, 'account/donthaveaccount.html')  # Rendu vers la page incitant à créer un compte

# Jeu du Puissance 4
def puissance4(request):
    context = default_context(request)
    return render(request, 'puissance4.html', context)

# Jeu du Calcul Mental
def calculmental(request):
    context = default_context(request)
    if request.method == 'POST':
        # Jeu récupère les requêtes POST de Samuel pour les mettres dans le modèle UserScore
        username = request.user.username
        score = request.POST.get("score")
        time = request.POST.get("time")
        hard = request.POST.get("isHard")

        hard = hard == "on"

        UserScore(user=request.user, games="calcul mental", score=score, time=time, hard=hard).save() #Ici je sauvegarde le modèle

    return render(request, 'calculmental.html', context) # Rendu basique

# Jeu du Morpion
def morpion(request):
    context = default_context(request)
    return render(request, 'morpion.html', context)
