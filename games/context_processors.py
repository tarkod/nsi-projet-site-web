from . import views


def foo(request):
    context = {"photo_index": None}
    if request.user.is_authenticated():
        userdata = views.getUserData(request)
        photo_url = ""
        try:
            photo_url = userdata.pdp.url
        except:
            print("photo null")
        context["photo_index"] = photo_url

    print(context)

    return context
