from django.urls import path

from . import views

urlpatterns = [
    path('', views.index),
    path('about/', views.about),
    path('account/', views.account),
    path('login/', views.loginPage),
    path('logout/', views.logoutPage),
    path('register/', views.register),
    path('games/', views.games),
    path('games/calculmental/', views.calculmental),
    path('games/puissance4/', views.puissance4),
    path('games/morpion/', views.morpion)
]
