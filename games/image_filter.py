from PIL import Image


def BlackAndWhiteFilter(path):                         #fon défini la fonction
    Imported_Image = Image.open(path)                  # importer l'image
    pixels = Imported_Image.load()                     # tableau de tuple qui renvoie les rgb

    for i in range(Imported_Image.size[0]):               #première boucle pour la hautre
        for j in range(Imported_Image.size[1]):             #deuxième boucle pour la longueur
            rgb = pixels[i, j]                             #tuple RGB
            noiretblanc = int((rgb[0] + rgb[1] + rgb[2]) / 3)                  #fonction native int qui définie la couleur des RGB
            pixels[i, j] = (noiretblanc, noiretblanc, noiretblanc)                      #onction native flottante "pixels"

    Imported_Image.save(path)

def Rouge(path):
    Imported_Image = Image.open(path)
    pixels = Imported_Image.load()

    for i in range(Imported_Image.size[0]):
        for j in range(Imported_Image.size[1]):
            rgb = pixels[i, j]
            pixels[i, j] = (256, rgb[1], rgb[2])

    Imported_Image.save(path)

def Vert(path):
    Imported_Image = Image.open(path)
    pixels = Imported_Image.load()

    for i in range(Imported_Image.size[0]):
        for j in range(Imported_Image.size[1]):
            rgb = pixels[i, j]
            pixels[i, j] = (rgb[0], 256, rgb[2])

    Imported_Image.save(path)

def Bleu(path):

    Imported_Image = Image.open(path)
    pixels = Imported_Image.load()

    for i in range(Imported_Image.size[0]):
        for j in range(Imported_Image.size[1]):
            rgb = pixels[i, j]
            pixels[i, j] = (rgb[0], rgb[1], 256)

    Imported_Image.save(path)

def Jaune(path):
    Imported_Image = Image.open(path)
    pixels = Imported_Image.load()

    for i in range(Imported_Image.size[0]):
        for j in range(Imported_Image.size[1]):
            rgb = pixels[i, j]
            pixels[i, j] = (240, 200, rgb[2])

    Imported_Image.save(path)



def Violet(path):
    Imported_Image = Image.open(path)
    pixels = Imported_Image.load()

    for i in range(Imported_Image.size[0]):
        for j in range(Imported_Image.size[1]):
            rgb = pixels[i, j]
            pixels[i, j] = (155, rgb[1], 221)

    Imported_Image.save(path)

def Turquoise(path):
    Imported_Image = Image.open(path)
    pixels = Imported_Image.load()

    for i in range(Imported_Image.size[0]):
        for j in range(Imported_Image.size[1]):
            rgb = pixels[i, j]
            pixels[i, j] = (rgb[1], 254, 247)

    Imported_Image.save(path)


def Rose(path):
    Imported_Image = Image.open(path)
    pixels = Imported_Image.load()

    for i in range(Imported_Image.size[0]):
        for j in range(Imported_Image.size[1]):
            rgb = pixels[i, j]
            pixels[i, j] = (255, rgb[1], 201)

    Imported_Image.save(path)

def Orange(path):
    Imported_Image = Image.open(path)
    pixels = Imported_Image.load()

    for i in range(Imported_Image.size[0]):
        for j in range(Imported_Image.size[1]):
            rgb = pixels[i, j]
            pixels[i, j] = (255, 94, rgb[0])

    Imported_Image.save(path)

























