from django import forms
from .models import UserData


class ImageForm(forms.Form):
    """Form for the image model"""
    pdp = forms.ImageField()  # Photo de profil
