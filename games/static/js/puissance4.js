const rouge = "rougeP4"
const jaune = "jauneP4"

let currentPlayer = rouge;
let grid = []
let victoire = false

function initGrid() {
    grid = []
    victoire = false

    for (let i = 0; i < 7; i++) {
        let array = []
        for (let j = 0; j < 6; j++) {
           array.push([])
        }
        grid.push(array)
    }
}



function getArray() {
    let array = []
    let grid = document.getElementById("grid").getElementsByTagName("div")
    for (let i = 0; i < grid.childNodes.length; i++) {
        let column = grid.childNodes[i].getElementsByTagName("div")
        let arr = []

        for (let j = 0; j < column.childNodes.length; j++)
            arr.push(column.childNodes[j])

        array.push(arr)
    }

    return array
}

function tokenPlacement() {
    let tokenCol = document.querySelectorAll("*:hover")[6];
    let tokenColId = tokenCol.id

    let freeSlot = slotCheck(tokenCol)
    if (freeSlot == null || victoire === true) return

    col = parseInt(tokenCol.id.charAt(4)) - 1
    lig = parseInt(freeSlot.charAt(3)) - 1

    grid[col][lig] = (currentPlayer === jaune) ? 1 : 0

    document.querySelectorAll("#" + tokenColId + " > #" + freeSlot)[0].style.backgroundImage = "url('../../static/img/" + currentPlayer + ".png')"

    if (victoryCheck(col, lig)) {
        victoire = true
        document.getElementById("turn").innerHTML = '<b>Victoire des </b> <img src="" alt="" id="playerPreview">'
        document.getElementById("playerPreview").src = "../../static/img/" + currentPlayer + ".png"
        document.getElementById("victory").innerHTML = '<button onClick="window.location.reload();" type = "submit" class="btn btn-secondary" > Relancer </button>'
    } else {
        swapPlayer()
        document.getElementById("playerPreview").src = "../../static/img/" + currentPlayer + ".png"
    }
}

function swapPlayer() {
    if (currentPlayer === rouge)
        currentPlayer = jaune
    else
        currentPlayer = rouge
}

function victoryCheck(c, l) {
    let token = grid[c][l]

    // COLUMN VICTORY
    if (l <= 2)
        if (grid[c][l+1] === token && grid[c][l+2] === token && grid[c][l+3] === token)
            return true;

    // LINE VICTORY
    let line_count = 1
    for (let i = c-1; i >= 0; i--)
        if (grid[i][l] === token)
            line_count += 1
        else
            break
    console.log(line_count, c)

    for (let i = c+1; i < 7; i++)
        if (grid[i][l] === token)
            line_count += 1
        else
            break

    if (line_count >= 4) return true

    // DIAGONAL VICTORY
    let diag_l_count = 1
    for (let i = 1; c+i < 7 && l+i <6; i++)
        if (grid[c+i][l+i] === token)
            diag_l_count += 1
        else
            break

    for (let i = 1; c-i >= 0 && l-i >= 0; i++)
        if (grid[c-i][l-i] === token)
            diag_l_count += 1
        else
            break

    if (diag_l_count >= 4) return true

    let diag_r_count = 1
    for (let i = 1; c+i < 7 && l-i >= 0; i++)
        if (grid[c+i][l-i] === token)
            diag_r_count += 1
        else
            break

    for (let i = 1; c-i >= 0 && l+i < 6; i++)
        if (grid[c-i][l+i] === token)
            diag_r_count += 1
        else
            break

    if (diag_r_count >= 4) return true

    return false
}

function slotCheck(tokenCol) {
    let nodes = tokenCol.childNodes;
    let reversedNodes = [];
    let freeSlot = null;

    for (let i = nodes.length - 1; i >= 0; i--) {
        if (nodes[i].nodeName.toLowerCase() == 'div') {
            reversedNodes.push(nodes[i]);
        }
    }

    reversedNodes.forEach(e => {
        if (e.style.backgroundImage == "") {
            if (freeSlot == null) {
                freeSlot = e.id;
            }
        }
    })

    return freeSlot;
}

function playerCheck() {

    let playerColor = document.getElementById('playerOneColor').value;
    let button = document.getElementById('generateP4');
    currentPlayer = playerColor;

    if (playerColor == "Choix du joueur 1") {
        button.disabled = true;
    } else {
        button.disabled = false;
    }
}

function launchP4() {
    document.getElementById('playerColor').style.display = 'none';
    document.getElementById('gameP4Container').style.display = 'block';
    document.getElementById('previewColor').style.display = 'block';
    document.getElementById("playerPreview").src = "../../static/img/" + currentPlayer + ".png"

    initGrid()
}
