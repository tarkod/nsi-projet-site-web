x = 0;
Y = 0;
operatorInt = 0;
isHardmode = false;
counterNumber = 1;
endgame = false;
goodAnswers = 0;
countdown = null;
totalSeconds = 0;
function generateOperator() {  //génére le type d'opération
    operatorInt = getRandomArbitrary(1, 4);
}

function swapVisibility() { //cache le bouton "générer un calcule" et montre le champ de texte
    document.getElementById('generateButton').style.display = 'none';
    document.getElementById('hardmode').style.display = 'none';
    document.getElementById('numberQuestion').style.display = 'none';
    if(!isHardmode) {
        document.getElementById('hardmodeLabel').innerText = "Normal";
    }

    document.getElementById('calculBlock').style.display = 'block';
}

function generateCalcul() { //génère le calcule aléatoirement en fct du choix de difficulté
    swapVisibility();

    generateOperator();
    y = getRandomArbitrary(1, 100);
    x = getRandomArbitrary(1, 100);

    if (isHardmode) {
        y = getRandomArbitrary(1, 1000);
        x = getRandomArbitrary(1, 1000);
    }

    if (operatorInt === 1) { //génération du type d'opération
        operator = " * ";
    } else if (operatorInt === 2) {
        operator = " + ";
    } else {
        operator = " - ";
    }

    document.getElementById('calcul').textContent = x + operator + y + " = ?";
}

checkbox = document.getElementById('hardmode');

checkbox.addEventListener('change', (event) => { //détecte le cochage du hard mode ou non
  isHardmode = event.currentTarget.checked;
})

function launchGame() { //lance le jeu avec génération de calcule + compte a rebours pour hardmode
    generateCalcul();
    launchCountdown();
}

function launchCountdown() { //lance le compte a rebours
        totalSeconds = (document.getElementById('numberQuestion').value)*12;
        seconds = (document.getElementById('numberQuestion').value)*12;
        countdown= setInterval(function() {
            seconds--;
            document.getElementById("countdown").textContent = seconds;
            if (seconds <= 0){
                endgame = true;
                clearInterval(countdown);
                scoreCalculator();
            }
        }, 1000);
}



function getRandomArbitrary(min, max) {  //crée un nombre aléatoire compris entre 2 valeurs
  return  Math.floor(Math.random() * (max - min) + min);
}

function checkAnswer() { //vérifie la réponse et donne si c'est juste ou non
    if (endgame == true) return;
    trueResult = calculate(x, y, operatorInt);
    userValue = document.getElementById('result').value;
    answerTag = document.getElementById('answer');
    answerTag.classList.add("bg-dark");
    isTrue = trueResult == userValue;

    if (isTrue) {
        answerTag.style.color = "green";
        answerTag.innerText = "Bonne réponse : " + trueResult;
        goodAnswers += 1
    } else {
        answerTag.style.color = "red";
        answerTag.innerText = " Mauvaise réponse : " + trueResult;
    }
     addAnswerToList(userValue, trueResult, isTrue);
     // ajout counter
    counterNumber += 1;
// if conuter est sup à nb calcul on fait de question
    let selectedQuestionNumberValue = document.getElementById('numberQuestion').value;
     if (selectedQuestionNumberValue >= counterNumber ){
             clearResult();
             generateCalcul();
     } else {
     endgame = true
     clearInterval(countdown);
     scoreCalculator();
     }

}
function clearResult () { //permet de supprimer la réponse précédante dans le champ d'envoie

    let userResult = document.getElementById('result');
    userResult.value = "";
    userResult.focus();
    userResult.select();
}


function calculate(x, y, operatorInt) { //permet d'effectuer le calcule en amont
    if (operatorInt === 1) {
        return x*y;
    } else if (operatorInt === 2) {
       return x+y;
    } else {
        return x-y;
    }
}

function addAnswerToList(answer,trueAnswer){ //crée une liste avec vrai ou faux dépendant de la réponse
    newLi = document.createElement('li');
    newLi.classList.add("list-group-item");

    if (!isTrue) {
        newLi.style.color = "red";
        newLi.textContent = "Incorrect";
    } else {
        newLi.style.color = "green";
        newLi.textContent = "Correct";
    }

    document.getElementById("listAnswers").append(newLi);
}

function questionVerification() { // Vérifie si un nombre de questions a créer a été séléctionné

    let questionNumber = document.getElementById('numberQuestion').value;
    let button = document.getElementById('generateTheButton');


    if (questionNumber == "Choix du nombre de calcul") {
        button.disabled = true;
   } else {

    button.disabled= false;
    }
}

function scoreCalculator () { //calcule le score en fonction du temps et des bonnes réponses
userScore = 0;
        if (seconds > (totalSeconds*(2/3))){
            userScore = goodAnswers * 3;
        } else if (seconds > (totalSeconds*(1/3))){
            userScore = goodAnswers * 2;
        } else {
        userScore = goodAnswers;
        }
scorePage(userScore)
}
function scorePage (userScore) { //envoie les donnés du score et du temps au formulaire post
userTime = totalSeconds - seconds
isHard = document.getElementById('isHard')
    document.getElementById('time').value =  userTime
    document.getElementById('score').value =  userScore
    if (isHardmode == true){
        isHard.checked = true;
    }
    document.getElementById('scoreFormSubmit').click()
}


