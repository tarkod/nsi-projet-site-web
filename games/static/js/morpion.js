let currentPawn = "croixMORPION";
let morpion_grid = []
let victory = false

const ROND  = "rondMORPION"
const CROIX = "croixMORPION"

function initMorpionGrid() {
    for (let i=0; i<9; i++)
        morpion_grid.push(0)
}

function pawnPlacement() {
    let signBox = document.querySelector(".caseMORPION:hover");
    let slot = checkSlot(signBox);
    if (slot == null || victory === true) return;

    if (currentPawn === ROND) {
        morpion_grid[slot] = 1
    } else {
        morpion_grid[slot] = 2
    }

    signBox.style.backgroundImage = "url('../../static/img/" + currentPawn + ".png')"

    console.log("slot", checkVictory(slot))

    if (checkVictory(slot)) {
        victory = true
        document.getElementById("previewSign").innerHTML = '<b>Victoire des </b> <img src="" alt="" id="playerPreview">'
        document.getElementById("playerPreview").src = "../../static/img/" + currentPawn + ".png"
        document.getElementById("victory").innerHTML = '<button onClick="window.location.reload();" type = "submit" class="btn btn-secondary" > Relancer </button>'
    } else {
        playerSwap()
        document.getElementById("previewPlayer").src = "../../static/img/" + currentPawn + ".png"
    }
}

function checkVictory(slot) {
    let pawn = morpion_grid[slot]

    // LINE VICTORY
    let count = 1
    for (let i = 3; slot + i < 9; i += 3) {
        if (morpion_grid[slot+i] === pawn) count++
    }

    for (let i = 3; slot - i >= 0; i += 3) {
        if (morpion_grid[slot-i] === pawn) count++
    }

    if (count === 3) return true

    // COLUMN VICTORY
    let divide = Math.floor(slot/3)
    if (morpion_grid[divide] === pawn && morpion_grid[divide+1] === pawn && morpion_grid[divide+2] == pawn)
        return true

    // DIAGONAL VICTORY
    switch (slot) {
        case 0:
            return morpion_grid[4] === pawn && morpion_grid[8] === pawn

        case 2:
            return morpion_grid[4] === pawn && morpion_grid[6] === pawn

        case 4:
            return (morpion_grid[0] === pawn && morpion_grid[8] === pawn)
                || (morpion_grid[2] === pawn && morpion_grid[6] === pawn)

        case 6:
            return morpion_grid[2] === pawn && morpion_grid[4] === pawn

        case 8:
            return morpion_grid[0] === pawn && morpion_grid[4] === pawn

        default:
            return false
    }
}

function playerSwap() {
    if (currentPawn === CROIX){
        currentPawn = ROND;
    } else {
        currentPawn = CROIX;
    }
}

function checkSlot (signBox) {
    let slot = signBox.id.charAt(4) - 1

    if (morpion_grid[slot] === 0)
        return slot
    else
        return null
}

function checkPlayer() {
    let playerSign = document.getElementById('playerOneSign').value;
    let buttonMor = document.getElementById('generateMORPION');
    currentPawn = playerSign;

    if (playerSign == "Choix du joueur 1") {
        buttonMor.disabled = true;
   } else {

    buttonMor.disabled= false;
    }
}

function launchMORPION() {
    document.getElementById('playerSign').style.display = 'none';
    document.getElementById('gameMorpionContainer').style.display = 'block';
    document.getElementById('previewSign').style.display = 'block';
    document.getElementById("previewPlayer").src= "../../static/img/" + currentPawn + ".png"

    initMorpionGrid()
}
